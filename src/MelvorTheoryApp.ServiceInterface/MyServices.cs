using System;
using ServiceStack;
using MelvorTheoryApp.ServiceModel;

namespace MelvorTheoryApp.ServiceInterface
{
    public class MyServices : Service
    {
        public object Any(Hello request)
        {
            return new HelloResponse { Result = $"Hello, {request.Name}!" };
        }
    }
}
