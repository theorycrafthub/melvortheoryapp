﻿using System.Security.Claims;
using MelvorTheoryApp.Core.Models.Identity.Security;

namespace MelvorTheoryApp.Core.Interfaces;

public interface IJwtService 
{
    /// <summary>
    /// Generates a new token.
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="claims"></param>
    /// <param name="now"></param>
    /// <returns>JwtAuthResult</returns>
    public JwtAuthResult GenerateToken(string userName, Claim[] claims, DateTime now);

    /// <summary>
    /// Refreshes an existing access token.
    /// </summary>
    /// <param name="refreshToken"></param>
    /// <param name="accessToken"></param>
    /// <param name="now"></param>
    /// <returns>JwtAuthResult</returns>
    public JwtAuthResult Refresh(string refreshToken, string accessToken, DateTime now);

}