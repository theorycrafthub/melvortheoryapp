﻿using MelvorTheoryApp.Core.Models.Identity;
using Microsoft.AspNetCore.Identity;

namespace MelvorTheoryApp.Core.Interfaces;

public interface IIdentityServerService
{
    public Task<ApplicationUser> GetByUserNameAsync(string userName);

    public Task<IdentityResult> CreateAsync(ApplicationUser user);

    public Task<bool> IsAuthorizedAsync(string userName, string password);
}