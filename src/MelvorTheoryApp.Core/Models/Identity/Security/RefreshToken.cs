﻿namespace MelvorTheoryApp.Core.Models.Identity.Security
{
    public class RefreshToken
    {
        public string UserName { get; set; }
        public string TokenString { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}
