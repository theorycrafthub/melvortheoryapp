﻿namespace MelvorTheoryApp.Core.Models.Identity.Security
{
    public class LoginResult
    {
        public string UserName { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
