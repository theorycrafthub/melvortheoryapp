﻿using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace MelvorTheoryApp.Core.Models.Identity.Security
{
    public class UserPasswordHasher : IPasswordHasher<ApplicationUser>
    {
        /// <summary>
        /// Hash a password for <see cref="ApplicationUser"/>
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns>string</returns>
        public string HashPassword(ApplicationUser user, string password)
        {
            using var sha256 = SHA256.Create();
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

            var hashStringBuilder = new StringBuilder();
            foreach (var h in hash)
            {
                hashStringBuilder.Append(h.ToString("x2"));
            }

            return hashStringBuilder.ToString();
        }

        /// <summary>
        /// Verifies if the provided password matches the user's current password
        /// </summary>
        /// <param name="user"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="providedPassword"></param>
        /// <returns><see cref="PasswordVerificationResult"/></returns>
        public PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string hashedPassword, string providedPassword)
        {
            return hashedPassword == HashPassword(user, providedPassword)
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.Failed;
        }
    }
}
