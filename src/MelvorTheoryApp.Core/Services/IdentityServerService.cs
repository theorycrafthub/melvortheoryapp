﻿using AspNetCore.Identity.Mongo.Stores;
using MelvorTheoryApp.Core.Interfaces;
using MelvorTheoryApp.Core.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MelvorTheoryApp.Core.Services
{
    public class IdentityServerService : IIdentityServerService
    {
        private readonly IMongoCollection<ApplicationRole> _roles;
        private readonly IMongoCollection<ApplicationUser> _users;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;

        private readonly IConfiguration _configuration;

        private UserStore<ApplicationUser, ApplicationRole, ObjectId> UserStore { get; set; }

        public IdentityServerService(IPasswordHasher<ApplicationUser> passwordHasher, IConfiguration configuration)
        {
            _configuration = configuration;
            var client = new MongoClient(_configuration["MongoDB:ConnectionString"]);
            var identityDatabase = client.GetDatabase("identity");

            _passwordHasher = passwordHasher;
            _users = identityDatabase.GetCollection<ApplicationUser>("users");
            _roles = identityDatabase.GetCollection<ApplicationRole>("roles");

            UserStore = new UserStore<ApplicationUser, ApplicationRole, ObjectId>(_users, _roles,
                new IdentityErrorDescriber());
        }

        public async Task<ApplicationUser> GetByUserNameAsync(string userName) =>
            await _users.Find(a => a.NormalizedUserName == userName.ToLower()).FirstOrDefaultAsync();

        public async Task<IdentityResult> CreateAsync(ApplicationUser user) => await UserStore.CreateAsync(user);

        public async Task<bool> IsAuthorizedAsync(string userName, string password)
        {
            var user = await GetByUserNameAsync(userName);
            var verificationResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);

            return verificationResult == PasswordVerificationResult.Success;
        }
    }
}
