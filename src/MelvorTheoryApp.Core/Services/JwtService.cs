﻿using System.Collections.Concurrent;
using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using MelvorTheoryApp.Core.Interfaces;
using MelvorTheoryApp.Core.Models.Identity.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace MelvorTheoryApp.Core.Services
{
    public class JwtService : IJwtService
    {
        public IImmutableDictionary<string, RefreshToken> UsersRefreshTokensReadOnlyDictionary =>
            _usersRefreshTokens.ToImmutableDictionary();

        private readonly ILogger<JwtService> _logger;
        private readonly ConcurrentDictionary<string, RefreshToken> _usersRefreshTokens;
        private readonly byte[] _secret;
        private IConfiguration _configuration;

        public JwtService(IConfiguration configuration, ILogger<JwtService> logger)
        {
            _logger = logger;
            _configuration = configuration;
            var jwtSecret = _configuration.GetSection("JwtSecurity").Value;
            _logger.LogDebug("The configured value for JwtSecret is {0}", jwtSecret);
            
            _usersRefreshTokens = new ConcurrentDictionary<string, RefreshToken>();
            _secret = Encoding.ASCII.GetBytes(jwtSecret);
        }

        public JwtAuthResult GenerateToken(string userName, Claim[] claims, DateTime now)
        {
            var jwtRegisteredClaimName = claims?.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Aud)?.Value;
            var shouldAddAudienceClaim =
                string.IsNullOrWhiteSpace(jwtRegisteredClaimName);
            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(_secret),
                SecurityAlgorithms.HmacSha256Signature);

            // TODO: check if 28 days is acceptable for a JWT 
            //var jwToken = new JwtSecurityToken("IssuerName", shouldAddAudienceClaim ? "AudienceName" : string.Empty,
            //    claims, expires: now.AddMinutes(10), signingCredentials: signingCredentials);
            var jwToken = new JwtSecurityToken(userName, shouldAddAudienceClaim ? jwtRegisteredClaimName : null, claims,
                expires: now.AddDays(28), signingCredentials: signingCredentials);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwToken);

            var refreshToken = new RefreshToken
            {
                UserName = userName,
                TokenString = GenerateRefreshTokenString(),
                ExpireAt = now.AddDays(28)
            };

            _usersRefreshTokens.AddOrUpdate(refreshToken.TokenString, refreshToken, (s, t) => refreshToken);

            _logger.LogDebug("The value for accessToken is {0}.", accessToken);
            _logger.LogDebug("The value for refreshToken is {0}.", refreshToken);

            return new()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
        }

        public JwtAuthResult Refresh(string refreshToken, string accessToken, DateTime now)
        {
            var (principal, jwToken) = DecodeJwtToken(accessToken);
            if (jwToken == null || !jwToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature))
            {
                _logger.LogError("An error of type: {0} occurred.", nameof(SecurityTokenException));
                _logger.LogDebug("The jwToken is {0}", jwToken == null ? "null" : "not null");
                _logger.LogDebug("The jwToken header algorithm value is {0}", jwToken?.Header.Alg);
                throw new SecurityTokenException("Invalid token.");
            }

            var userName = principal.Identity?.Name;
            if (!_usersRefreshTokens.TryGetValue(refreshToken, out var existingRefreshToken))
            {
                _logger.LogError("An error of type: {0} occurred.", nameof(SecurityTokenException));
                _logger.LogDebug("The refreshToken: {0} was not found.", refreshToken);
                throw new SecurityTokenException("Invalid token.");
            }

            if (existingRefreshToken.UserName != userName || existingRefreshToken.ExpireAt <= now)
            {
                _logger.LogError(
                    "The existingRefreshToken is either a foreign refreshToken or the refreshToken already expired.");
                throw new SecurityTokenException("Invalid token.");
            }

            return GenerateToken(userName, principal.Claims.ToArray(), now);
        }

        private (ClaimsPrincipal, JwtSecurityToken?) DecodeJwtToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                _logger.LogError("The {0} instance referenced to null.", nameof(token));
                throw new ArgumentNullException(nameof(token));
            }


            var principal = new JwtSecurityTokenHandler()
                .ValidateToken(token,
                    new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = "IssuerName",
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(_secret),
                        ValidateAudience = true,
                        ValidAudience = "AudienceName",
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(1)
                    }, out var validatedToken);

            return (principal, validatedToken as JwtSecurityToken);
        }

        private string GenerateRefreshTokenString()
        {
            var randomNumber = new byte[32];
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(randomNumber);

            return Convert.ToBase64String(randomNumber);
        }
    }
}
