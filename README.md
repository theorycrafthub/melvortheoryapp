# MelvorTheoryApp

This project provides a svelte WebApplication which grants a user the possibility to load his/her Melvor Idle save game file and calculate some values which can be used for some advanced theorycraft.



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)